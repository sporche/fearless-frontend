function createCard(pictureUrl, title, locationTag, description,startDate, endDate) {
  return `
    <div class="col-md-4 col-lg-3 mb-4">
    <div class="card shadow">
      <img src="${pictureUrl}" class="card-img-top" alt="${title}">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-location">${locationTag}</h6>
        <p class="card-text">${description}</p>
        <p class="card-footer">${startDate.toLocaleDateString()}-${endDate.toLocaleDateString()}</p>

        </div>
      </div>
    </div>
    `
}

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Handle the case when the response is not okay
      } else {
        const data = await response.json();

        const cardContainer = document.getElementById('conference-cards');


        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const locationTag = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const cardBody = document.querySelector('.card-body');
            createCard(pictureUrl, title, locationTag, description, startDate, endDate)
            const cardHTML = createCard(pictureUrl, title, locationTag, description, startDate, endDate)
            cardContainer.innerHTML += cardHTML;
          }
        }
      }
    } catch (e) {`
      <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
      </div>
    `}
  });
